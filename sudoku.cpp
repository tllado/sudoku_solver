#include <iostream>

#define TEST 2

const int easy[81]{
0,1,6, 4,2,0, 0,3,0,
0,0,0, 0,1,0, 2,7,0,
2,0,3, 0,6,9, 4,0,0,

8,5,0, 0,0,0, 7,4,1,
6,4,0, 1,0,8, 9,0,0,
0,3,0, 0,4,0, 0,0,8,

0,0,0, 0,3,2, 0,5,7,
9,0,5, 6,0,0, 0,0,4,
3,7,1, 5,0,0, 0,0,0};

const int medium[81]{
0,0,0, 0,0,0, 0,0,0,
0,0,0, 1,5,8, 0,0,0,
0,0,0, 9,0,3, 0,6,1,

0,0,0, 3,8,1, 5,9,0,
2,0,0, 0,0,0, 3,0,0,
0,0,9, 0,0,0, 7,0,6,

0,0,7, 0,0,0, 0,4,0,
0,3,5, 0,4,0, 6,0,0,
0,0,0, 2,0,9, 0,0,0};

const int hard[81]{
0,0,0, 0,7,4, 0,0,5,
0,0,0, 0,0,0, 2,0,0,
0,0,0, 3,0,5, 0,7,6,

0,0,9, 4,0,2, 0,8,0,
1,8,0, 0,6,0, 0,0,0,
0,3,0, 0,0,0, 0,0,0,

2,0,0, 5,0,0, 8,0,0,
3,5,0, 0,0,1, 0,0,0,
0,0,8, 0,0,0, 9,0,0};

struct Cell {
  int value {0};
  bool possibility[81] {true, true, true, true, true, true, true, true, true};
};

class Sudoku {
public:
  void displayBoard(void) const;
  void displayPossibilities(void) const;
  void getBoard(void);
  int numFound(void) const;
  int numPossibilities(void) const;
  void setCell(const int i, const int v);
  void checkLonePossibilities(void);
  void checkLoneCells(void);
private:
  void clearBlock(const int i, const int p);
  void clearColumn(const int i, const int p);
  void clearCell(const int i);
  void clearRow(const int i, const int p);
  Cell board[81];
};

void Sudoku::displayBoard(void) const {
  std::cout << board[0].value;
  for (int i = 1; i < 81; i++) {
    if ((i % 27) == 0) {
      std::cout << std::endl << std::endl;
    } else if ((i % 9) == 0) {
      std::cout << std::endl;
    } else if ((i % 3) == 0) {
      std::cout << "   ";
    } else {
      std::cout << "  ";
    }
    std::cout << board[i].value;
  }
  std::cout << std::endl;
}

void Sudoku::displayPossibilities(void) const {
  for (int i = 0; i < 81; i++) {
    if ((i % 9) == 0) {
      std::cout << std::endl;
    }
    std::cout << board[i].value << "  ";
    for(int p = 0; p < 9; p++) {
      std::cout << board[i].possibility[p] << " ";
    }
    std::cout << std::endl;
  }
}

void Sudoku::getBoard(void) {
  #if TEST
    for (int i = 0; i < 81; i++) {
      switch (TEST) {
        case 1:
          setCell(i, easy[i]);
          break;
        case 2:
          setCell(i, medium[i]);
          break;
        case 3:
          setCell(i, hard[i]);
          break;
        default:
          break;
      }
    }
  #else
      int v;
    std::cout << "Enter the beginning board with 0's for empty cells" << std::endl;
    for (int i = 0; i < 81; i++) {
      do {
        std::cout << "Enter [" << ((i / 9) + 1) << "," << ((i % 9) + 1) << "] ";
        std::cin >> v;
      } while (v > 9);
      setCell(i, v);
    }
    std::cout << std::endl;
  #endif
}

int Sudoku::numFound(void) const {
  int num_found {0};

  for(int i = 0; i < 81; i++) {
    num_found += (board[i].value != 0);
  }

  return num_found;
}

int Sudoku::numPossibilities(void) const {
  int num_p {0};

  for(int i = 0; i < 81; i++) {
    for(int p = 0; p < 9; p++) {
      num_p += board[i].possibility[p];
    }
  }

  return num_p;
}

void Sudoku::setCell(const int i, const int v) {
  if(v != 0) {
    board[i].value = v;
    clearCell(i);
    clearBlock(i, (v - 1));
    clearColumn(i, (v - 1));
    clearRow(i, (v - 1));
  }
}

void Sudoku::checkLonePossibilities(void) {
  for(int i = 0; i < 81; i++) {
    int num_p {0};
    int last_p {0};

    if(board[i].value == 0) {
      for(int p = 0; p < 9; p++) {
        if(board[i].possibility[p]) {
          num_p++;
          last_p = p;
        }
      }

      if(num_p == 1) {
        #if TEST
          std::cout << i << " can only be " << (last_p + 1)8 << std::endl;
        #endif

        setCell(i, (last_p + 1));
      }
    }
  }
}

void Sudoku::checkLoneCells(void) {
  for (int p = 0; p < 9; p++) {
    // Check rows
    for (int rb = 0; rb < 81; rb += 9) {
      int num_cells {0};
      int last_cell {0};
      for (int i = rb; i < (rb + 9); i++) {
        if (board[i].possibility[p]) {
          num_cells++;
          last_cell = i;
        }
      }

      if (num_cells == 1) {
        #if TEST
          std::cout << last_cell << " is only " << (p + 1) << " in row" << std::endl;
        #endif

        setCell(last_cell, (p + 1));
      }
    }

    // Check columns
    for (int cb = 0; cb < 9; cb++) {
      int num_cells {0};
      int last_cell {0};
      for (int i = cb; i < (cb + 72); i += 9) {
        if (board[i].possibility[p]) {
          num_cells++;
          last_cell = i;
        }
      }

      if (num_cells == 1) {
        #if TEST
          std::cout << last_cell << " is only " << (p + 1) << " in column" << std::endl;
        #endif

        setCell(last_cell, (p + 1));
      }
    }

    // Check blocks
    for (int rb = 0; rb < 9; rb += 3) {
      for (int cb = 0; cb < 9; cb += 3) {
        int num_cells {0};
        int last_cell {0};
        for (int rx = rb; rx < (rb + 3); rx++) {
          for (int cx = cb; cx < (cb + 3); cx++) {
            const int i = rx * 9 + cx;

            if (board[i].possibility[p]) {
              num_cells++;
              last_cell = i;
            }
          }
        }

        if (num_cells == 1) {
          #if TEST
            std::cout << last_cell << " is only " << (p + 1) << " in block" << std::endl;
          #endif

          setCell(last_cell, (p + 1));
        }
      }
    }
  }
}

void Sudoku::clearBlock(const int i, const int p) {
  const int r = i / 9;
  const int c = i % 9;
  const int rb = (r / 3) * 3;
  const int cb = (c / 3) * 3;

  for (int rx = rb; rx < (rb + 3); rx++) {
    for (int cx = cb; cx < (cb + 3); cx++) {
      board[rx * 9 + cx].possibility[p] = false;
    }
  }
}

void Sudoku::clearColumn(const int i, const int p) {
  const int c = i % 9;

  for(int r = 0; r < 9; r++) {
    board[(r * 9) + c].possibility[p] = false;
  }
}

void Sudoku::clearCell(const int i) {
  for(int p = 0; p < 9; p++) {
    board[i].possibility[p] = false;
  }
}

void Sudoku::clearRow(const int i, const int p) {
  const int r = i / 9;

  for(int c = 0; c < 9; c++) {
    board[(9 * r) + c].possibility[p] = false;
  }
}

int main () {
  Sudoku sudoku;
  sudoku.getBoard();

  std::cout << std::endl;
  std::cout << "Beginning Board" << std::endl;
  sudoku.displayBoard();

  #if TEST
    std::cout << "num found " << sudoku.numFound() << std::endl;
    std::cout << "num possibilities " << sudoku.numPossibilities() << std::endl;
  #endif

  int num_p {81*9};
  int num_p_previous {81*9};
  do {
    num_p_previous = num_p;
    sudoku.checkLonePossibilities();
    sudoku.checkLoneCells();
    num_p = sudoku.numPossibilities();

    #if TEST
      std::cout << std::endl;
      sudoku.displayBoard();
      std::cout << "num found " << sudoku.numFound() << std::endl;
      std::cout << "num possibilities " << sudoku.numPossibilities() << std::endl;
    #endif
  } while ((num_p != num_p_previous) && (num_p != 0));

  std::cout << std::endl;
  if(sudoku.numFound() == 81) {
    std::cout << "Solved Board" << std::endl;
  } else {
    std::cout << "Couldn't solve ... sorry ... " << (81 - sudoku.numFound()) << " remaining" << std::endl;
  }
  sudoku.displayBoard();

  #if TEST
    std::cout << "num found " << sudoku.numFound() << std::endl;
    std::cout << "num possibilities " << sudoku.numPossibilities() << std::endl;
  #endif

  return 0;
}
